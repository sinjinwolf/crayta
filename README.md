This project is for tracking progress of projects/packages/etc. in [Crayta](https://crayta.com)

Start Date: 2022-02

# Organization

| Area     | Description |
| -------- | ----------- |
| Issues   | One issue per package
| Snippets | Package documentation and code
| Wiki     | ideas, brainstorming, etc.


# Support the following packages

| Package Name                    | Dependencies |    |
| ------------------------------- | ------------ | -- |
| Basic Juke Box                  |
| Checkpoint Homepoint            |
| Division Healthbar              |
| Equip and Stow                  |
| Event Trigger                   | Interact Prompt | Open Close Animation Events
| Event Trigger Power Cube Add-on | Interact Prompt
| Interact Prompt                 |
| Lift Platform                   | Interact Prompt | Open Close Animation Events
| Lift Two                        | Interact Prompt
| Loot One                        | Interact Prompt | Open Close Animation Events
| Melee Accessibility             |
| Open Close Animation Events     | 
| Physics Gun Accessibility       | 
| Pickup Template Scheduler       |
| Trigger Cloak                   |
| Underwater Camera               | (not published)
